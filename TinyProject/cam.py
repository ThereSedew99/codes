import numpy as np
import cv2

capture = cv2.VideoCapture(0)

while 1:
	ret_val, frame = capture.read() 
	cv2.imshow("w1", frame)

	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

capture.release()
cv2.destroyAllWindows()
