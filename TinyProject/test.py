import serial
import signal
import sys
import numpy as np
import cv2
from Queue import Queue
from threading import Thread

ser = serial.Serial('/dev/ttyACM0', 9600)
playCam = 1
threadKill = 0

def gracefulEnd():
	global threadKill

	threadKill = 1
	capture.release()
	#outVideo.release()
	cv2.destroyAllWindows()
	print "[*] Webcam destroyed\n"
	
def handle_ctrl_c(signal,frame):
	print "[*] Stop script"
	gracefulEnd()
	t.join()
	print "[*] Thread end\n"
	sys.exit(0)

signal.signal(signal.SIGINT, handle_ctrl_c)

def read_serial():
	oldX = ""
	oldY = ""
	oldZ = ""

	counter = 0
	global playCam
	global threadKill
	
	while 1:
		if threadKill == 1 :
			break

		readData = ser.readline()
		print "%s\n" % readData

		components = readData.split() 
		if components[0] == 'a:' :
			print components
			axeX = components[1]
			axeY = components[2]
			axeZ = components[3]
	
			if oldX == axeX and oldY == axeY and oldZ == axeZ :
				print "same\n"
				counter = counter+1
			else :
				print "diff\n"
				oldX = components[1] 
				oldY = components[2] 
				oldZ = components[3] 
				counter=0
			print counter

			if playCam == 1 and counter > 2 :
				print "[*] stop web cam\n"
				counter = 0
				playCam = 0	
			else :
				print "[*] run web cam\n"
				playCam = 1

t = Thread(target=read_serial)
t.start()
print "[*] start thread\n"

capture = cv2.VideoCapture(0)
#fourcc = cv2.VideoWriter_fourcc(*'XVID')
#fourcc = cv2.cv.CV_FOURCC(*'XVID')
#outVideo = cv2.VideoWriter('output.avi', fourcc, 20.0, (640,480))
print "[*] run cam_repeat\n"

while 1:
	if cv2.waitKey(1) & 0xFF == ord('q'):
		print "[*] End main loop\n"
		threadKill = 1
		break

	if playCam == 1 :
		ret_val, frame = capture.read()
		cv2.imshow("w1", frame)
		#outVideo.write(frame)
	
	elif playCam == 0 :
		cv2.destroyAllWindows()
		playCam = 2


gracefulEnd()
t.join()
print "[*] Thread end\n"

