import numpy as np
import cv2 
from matplotlib import pyplot as plt

filename = "k1.jpg"

img_ori = cv2.imread(filename) 
img_Blur = cv2.medianBlur(img_ori, 15)
#img_BW = cv2.GaussianBlur(img_ori, (5,5), 0)
img_BW = cv2.cvtColor(img_Blur, cv2.COLOR_BGR2GRAY) 

ret,thresh1 = cv2.threshold(img_BW,215,255,1) 
#ret,thresh1 = cv2.threshold(img_BW,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU) 

#square image kernel used for erosion
kernel = np.ones((5,5),np.uint8) 
erosion = cv2.erode(thresh1, kernel,iterations = 2) 
edges = cv2.Canny(erosion, 10,250)

#refines all edges in the binary image
#opening = cv2.morphologyEx(erosion, cv2.MORPH_OPEN, kernel)
#this is for further removing small noises and holes in the image
closing = cv2.morphologyEx(edges, cv2.MORPH_CLOSE, kernel) 

#find contours with simple approximation
_, contours, hierarchy = cv2.findContours(closing,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE) 
#_, contours, hierarchy = cv2.findContours(opening,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE) 

areas = [] 
#list to hold all areas

for contour in contours:
  ar = cv2.contourArea(contour)
  peri = cv2.arcLength(contour, True)
  approx = cv2.approxPolyDP(contour, 0.02*peri, True)
  print "Peri : %d " % (peri)
  cv2.drawContours(img_ori, [approx], -1, (0, 255, 0), 2)
  areas.append(ar)

#index of the list element with largest area
max_area = max(areas)
max_area_index = areas.index(max_area) 

#largest area contour
cnt = contours[max_area_index] 
cv2.drawContours(img_ori, [cnt], -1, (255, 0, 0), 3, maxLevel = 0)

images = [img_BW, erosion, edges, closing]
titles = ["original", "erosion", "edges", "Contour" ]

#for i in xrange(4):
#	plt.subplot(2,2,i+1)
#	plt.imshow(images[i], 'gray')
#    	plt.title(titles[i])
#    	plt.xticks([]),plt.yticks([])
#plt.show()

area = cv2.contourArea(cnt)
hull = cv2.convexHull(cnt)
hull_area = cv2.contourArea(hull)
solidity = float(area)/hull_area
print "solidity %s" % solidity

M = cv2.moments(cnt)
cX = int(M["m10"] / M["m00"])
cY = int(M["m01"] / M["m00"])
cv2.circle(img_ori, (cX, cY), 7, (255, 255, 255), -1)
cv2.putText(img_ori, "center", (cX - 20, cY - 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)

ellipse = cv2.fitEllipse(cnt)
img = cv2.ellipse(img_ori, ellipse, (0,0,255),2)
print "virtual ellipse"
print ellipse

rect = cv2.minAreaRect(cnt)
print rect 
box = cv2.boxPoints(rect)
box = np.int0(box)
im = cv2.drawContours(img_ori, [box], 0, (0,0,255),2)
print "virtual box"
print box

x,y,w,h = cv2.boundingRect(box)
aspect_ratio = float(w)/h
print "aspect ratio"
print aspect_ratio
#img = cv2.rectangle(img_ori,(x,y),(x+w,y+h),(0,255,255),2)

cv2.imshow('cleaner', img)
cv2.waitKey(0)

cv2.imwrite("P_"+filename, img) 
cv2.destroyAllWindows()
