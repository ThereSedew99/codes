#include <iostream>

using namespace std;

struct BinaryTreeNode {

	BinaryTreeNode *parent;
	BinaryTreeNode *left;
	BinaryTreeNode *right;

	int value;
};


BinaryTreeNode* GetNext(BinaryTreeNode * aNode){
	BinaryTreeNode *parentNode = NULL;
	BinaryTreeNode *curNode = NULL;
	BinaryTreeNode *pRet = NULL;

	if ( aNode == NULL ) return NULL;

	if ( aNode->right == NULL ) {
		if ( aNode->parent != NULL ){
			curNode = aNode;
			parentNode = aNode->parent;

			if ( parentNode->left==aNode ) return parentNode;
			else if ( parentNode->right==aNode ) {
				curNode = parentNode;
				while ( curNode->parent!=NULL 
					&& curNode->parent->left!=curNode ){
					curNode = curNode->parent;
				}
				pRet= curNode->parent;
			}
		}
		// else NULL

	} else {
		curNode = aNode->right;
		while (curNode->left!=NULL){
			curNode = curNode->left;
		}
		pRet = curNode;
	}

	return pRet;
}

int main(){






}


