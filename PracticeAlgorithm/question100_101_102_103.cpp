#include <iostream>

using namespace std;

int add(int a, int b){

	int sum=0;
	int carry=0;

	do {
		sum = a ^ b;
		carry = (a & b)	<< 1;
		a = sum;
		b= carry;
	} while ( carry!=0 );

	return sum;
}

int sub(int a, int b){

	int minusB = ~b+1;

	return add(a, minusB);
}

int multi(int a, int b){
	bool isMinus = false;
	int shift = 0x1;
	int sum = a;

	if ( a==0 || b==0 ) return 0;

	if ( (a<0&&b<0) && (a>0&&b>0) ) isMinus = false;
	else isMinus = true;

	while ( b!=0 ){
		if ( b&0x1 ) {
			a = a<<shift;
			sum += a;
		}

		b = b >> 1;
		shift = shift << 1;
	}

	if ( isMinus==true ){
		sum = add(~sum, 1);
	}

	return sum;
}

int main(){


	int x=2;
	unsigned int y=1;

	while (1){
		x-=3;
		
		if ( 0<sub(y,x) ) break;
		cout <<"*";
	}

	cout << add(10,7) << endl;
	cout << add(-10,7) << endl;
	cout << add(0,0) << endl;
	cout << add(-1,-1) << endl;


	cout << sub(10,7) << endl;
	cout << sub(-10,7) << endl;
	cout << sub(0,0) << endl;
	cout << sub(-1,-1) << endl;

	return 0;
}
