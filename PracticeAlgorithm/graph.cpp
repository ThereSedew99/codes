#include <iostream>
#include <list>
#include <queue>
using namespace std;

// virtual function for multipe type of graphs
class Graph {

protected:
	bool* adjArray = NULL;
	int vertexCnt;
	int edgeCnt;

public:
	Graph(int V);
	//using virtual for the deconstructure of mother class
	virtual ~Graph();

	void virtual addEdge(int v, int w);
	int adj(int v, list<int>* adjList);
	int adj(int v);
	int V(){ return vertexCnt; }
	int E(){ return edgeCnt; }
	//void IsGraph();
	void print();
};

class DirectGraph : public Graph {

public:
	DirectGraph(int V) : Graph(V) {
		cout << "constructor of DirectGraph\n";
  	}

	void virtual addEdge(int v, int w);


	//DirectGraph revert();
};

void DirectGraph::addEdge(int v, int w){
	if ( *(adjArray+(v+(w*vertexCnt))) != true ) {
		*(adjArray+(v+(w*vertexCnt))) = true;
		edgeCnt++;
	}
}

Graph::Graph(int V){
	cout << "contructoer of Graph\n";

	if (adjArray != NULL) delete[] adjArray;
	adjArray = new bool[V*V];
	// prepare low memory condition

	vertexCnt = V;
}

Graph::~Graph(){
	delete[] adjArray;
	cout << "destroy\n";
}

void Graph::addEdge(int v, int w){
	if ( (*(adjArray+(v+(w*vertexCnt))) != true) || 
	     (*(adjArray+(w+(v*vertexCnt))) != true) ) {
		*(adjArray+(v+(w*vertexCnt))) = true;
		*(adjArray+(w+(v*vertexCnt))) = true;
		edgeCnt++;
	}
}

int Graph::adj(int v){
	int ret=0;
	for (int i=0;i<V();i++){
		if ( *(adjArray+(v+ i*vertexCnt)) == true) {
			cout << v << " - " << i <<"\n";
			ret++;
		}
	}
	return ret;
}

//function overloading
int Graph::adj(int v, list<int>* adjList ){
	int ret=0;

	for (int i=0;i<V();i++){
		if ( *(adjArray+(v+ i*vertexCnt)) == true ) { 
			adjList->push_front(i);
			ret++;
		}

	}

	return ret;
}

void Graph::print(){

	for (int i=0;i<V();i++){
		for (int j=0;j<V();j++){
			if ( *(adjArray+(i+j*vertexCnt)) == true){
				cout << i << "-" << j << "\n";
			}
		}
	}

}

int degree(Graph& pG, int v){
	//return (*G).adj(v);
	return pG.adj(v);
}

int maxDegree(Graph& pG){
	int max=0;
	int vDegree=0;

	for (int v=0; v < pG.V(); v++){
		vDegree = degree(pG,v);
		if ( max < vDegree ) max = vDegree;
	}

	return max;
}

int averageDegree(Graph& pG){
	return 2.0* pG.E() / pG.V(); 
}

int numberOfSelfLoops(Graph& pG){
	int ret=0;
	list<int> adjList;
	list<int>::iterator itor;

	for (int v=0;v < pG.V();v++){
		adjList.clear();
		pG.adj(v,&adjList);		
		for (itor=adjList.begin(); itor !=adjList.end();itor++){
			cout << v << " check self : " << *itor << "\n";
			if ( v == (*itor) ) { cout << "*"; ret++; }
		}
	}

	return ret/2;
}

class DirectedDFS 
{
	bool* marked = NULL;
	int vCnt;
public:
	DirectedDFS ( Graph& grap, int s );
	~DirectedDFS();

	int dfs(Graph& grap, int v);

private:
	int visited(int v) { 
		if ( marked != NULL )
			return marked[v]; 
		else return -1;
	}

	int reachable(int v); 

};

DirectedDFS::DirectedDFS(Graph& grap, int s){
	vCnt = grap.V();

	if ( marked != NULL ) delete[] marked;
	marked = new bool[vCnt];
	// prepare low memory condition


	cout << "* DFS for " << s << "\n";
	dfs(grap, s);
	cout << "* Reachable : " << "\n";
	reachable(s);
}

DirectedDFS::~DirectedDFS(){
	delete[] marked;
}

int DirectedDFS::dfs(Graph& grap, int v){
	list<int> adjList;
	list<int>::iterator itor;

	marked[v] = true;

	grap.adj(v, &adjList);
	for ( itor = adjList.begin(); itor!=adjList.end(); itor++ ){
		if ( visited(*itor) != true ){
			cout << v << " - " << *itor << "\n";
			dfs(grap, *itor);
		}
	}
}

int DirectedDFS::reachable(int v){
	cout << "* reachable ";
	for (int index=0; index < vCnt; index++){
		if ( visited(index) == true ) cout << "-" << index ;
	}
	cout << "\n";

}

class DirectedBFS {
	int* edgeTo = NULL;
	int* distTo = NULL;
	int* marked = NULL;
	int vCnt;

	queue<int> adjQueue;

public:
	DirectedBFS(Graph& grap, int s);
	DirectedBFS(Graph& grap, list<int>& multiS);
	// problem!!! what if the second parameter has zero
	~DirectedBFS();

	void DirectedBFS_init(Graph& grap);
	void bfs(Graph& grap);
	void reachable();
};

void DirectedBFS::DirectedBFS_init(Graph& grap){
	vCnt = grap.V();

	if ( edgeTo != NULL ) delete[] edgeTo;
	if ( distTo != NULL ) delete[] distTo;
	if ( marked != NULL ) delete[] marked;
	edgeTo = new int[vCnt];
	distTo = new int[vCnt];
	marked = new int[vCnt];
	// prepare low memory condition

	for (int i=0;i<vCnt; i++){
		edgeTo[i] = -1;
	}
}

DirectedBFS::DirectedBFS(Graph& grap, int s){
	DirectedBFS_init(grap);

	cout << "* BFS for " << s << "\n";	
	adjQueue.push(s);
	distTo[s] = 0;
	marked[s] = true;

	bfs(grap);
	cout << "* reachable : \n";
	reachable();
}

DirectedBFS::DirectedBFS(Graph& grap, list<int>& multiS){
	DirectedBFS_init(grap);

	for (list<int>::iterator itor = multiS.begin(); itor!=multiS.end(); itor++){
		adjQueue.push(*itor);
		distTo[*itor] = 0;
		marked[*itor] = true;
	}

	cout << "* BFS for multiS \n";
	bfs(grap);
	cout << "* reachable : \n";
	reachable();
}

DirectedBFS::~DirectedBFS(){
	delete[] edgeTo;
	delete[] distTo;
	cout << "deconstructor of DirecedBFS\n";
}

void DirectedBFS::bfs(Graph& grap){
	list<int> adjList;
	list<int>::iterator itor;
	int v;

	while ( !adjQueue.empty() ) {

		v = adjQueue.front();
		adjQueue.pop();

		grap.adj(v, &adjList);
		if ( !adjList.empty() ) {
			for ( itor = adjList.begin(); itor != adjList.end(); itor++ ){
				if ( marked[*itor] != true ){
					adjQueue.push(*itor);	
					edgeTo[*itor] = v;		
					distTo[*itor] = distTo[v]+1;
					marked[*itor] = true;
				}
			}
		}
	}
}

void DirectedBFS::reachable(){

	for (int i=0;i<vCnt;i++){
		cout << i << " - " << edgeTo[i] << " : " << distTo[i] << "\n";
	}
}

int GraphTest(){
	Graph grap(13); 

	cout << "Graph test\n";

	grap.addEdge(0,5);
	grap.addEdge(4,3);
	grap.addEdge(0,1);
	grap.addEdge(9,12);
	grap.addEdge(6,4);
	grap.addEdge(5,4);
	grap.addEdge(0,2);
	grap.addEdge(11,12);
	grap.addEdge(9,10);
	grap.addEdge(0,6);
	grap.addEdge(7,8);
	grap.addEdge(9,11);
	grap.addEdge(5,3);

	grap.print();

	cout << "\n";
	//If grap is not transfer as pointer, the class will be copied
	//and will call destructure when the function ended.
	cout << "degree: " << degree(grap,0) << "\n";

	cout << "\n";
	grap.adj(0);

	cout << "\n";
	cout << "self loop : " << numberOfSelfLoops(grap) <<"\n";
	cout << "end\n";
	return 0;
}

int DirectGraphTest(){

	DirectGraph grap(13);

	grap.addEdge(0,5);
	grap.addEdge(0,1);
	grap.addEdge(2,0);
	grap.addEdge(2,3);
	grap.addEdge(3,5);
	grap.addEdge(3,2);
	grap.addEdge(4,3);
	grap.addEdge(4,2);
	grap.addEdge(5,4);
	grap.addEdge(6,9);
	grap.addEdge(6,4);
	grap.addEdge(6,8);
	grap.addEdge(6,0);
	grap.addEdge(7,6);
	grap.addEdge(7,9);
	grap.addEdge(8,6);
	grap.addEdge(9,11);
	grap.addEdge(9,10);
	grap.addEdge(10,12);
	grap.addEdge(11,4);
	grap.addEdge(11,12);
	grap.addEdge(12,9);

	grap.print();
	cout << "self loop : " << numberOfSelfLoops(grap) << "\n";

	grap.adj(0);

	DirectedDFS(grap,0);

	DirectGraph grapBSF(6);
	grapBSF.addEdge(0,1);
	grapBSF.addEdge(0,2);
	grapBSF.addEdge(1,2);
	grapBSF.addEdge(2,4);
	grapBSF.addEdge(3,2);
	grapBSF.addEdge(3,5);
	grapBSF.addEdge(4,3);

	DirectedBFS(grapBSF, 0);

	list<int> multiS;
	multiS.push_front(0);
	multiS.push_front(3);

	DirectedBFS(grapBSF, multiS);
}

int main(){
	//GraphTest();
	DirectGraphTest();

	return 0;
}
