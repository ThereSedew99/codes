#include <iostream>
#include <bitset>

using namespace std;

int countOneBit(int input){

	int temp = input;
	int cnt = 0;

	while ( temp ) {	
		temp = (temp-1)&temp;
		cnt++;
	}

	return cnt;
}

int main(){
	int a=10, b=13;

	cout << countOneBit( a^b ) << endl;
	return 0;
}
