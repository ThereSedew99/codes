#include <iostream>

using namespace std;

bool checkBSTcore(BinaryTreeNode* pRoot, int min, int max){


	if ( pRoot==NULL ) return true; 
	
	if ( pRoot->value <= min || pRoot->value >= max ) return false;

	return checkBST(pRoot->left, min, pRoot->value) && checkBST(pRoot->right, pRoot->value, max);
}

int checkBST(BinaryTreeNode* pRoot){

	int min = numeric_limits<int>::min();
	int max = numeric_limits<int>::max();

	return checkBSTcore(pRoot, main, max);	
}
