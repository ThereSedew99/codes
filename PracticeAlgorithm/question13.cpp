#include <iostream>
#include <stack>
#include <cstring>

using namespace std;

typedef struct ListNode {
	ListNode* m_nNext;
	int m_nValue;
}aNode;

int insertListNode(ListNode** pHead, int value) {
	ListNode *tempNode=NULL;

	//tempNode = (ListNode*)malloc(sizeof(ListNode));
	tempNode = new ListNode;

	if (tempNode==NULL) return 0;

	tempNode->m_nValue = value;
	tempNode->m_nNext = NULL;

	if ( *pHead == NULL ) {
		*pHead = tempNode;
	} else {
		tempNode->m_nNext = *pHead;	
		*pHead = tempNode;
	}

	return 1;
}

int freeListNodeAll(ListNode** pHead){
	ListNode *tempNode=NULL;

	while (*pHead!=NULL){
		tempNode = *pHead;
		*pHead = (*pHead)->m_nNext;
		delete tempNode;
	}

	return 0;
}

int PrintList(ListNode* pHead){
	while ( pHead!=NULL ){

		cout << pHead->m_nValue << "\t" ;
		pHead = pHead->m_nNext;
	}
	cout << endl;

	return 1;
}

void PrintListReverse_Iteratively(ListNode* pHead){
	std::stack<ListNode*> nodes;
	ListNode* pNode = pHead;

	while (pNode!=NULL){
		nodes.push(pNode);
		pNode = pNode->m_nNext;
	}

	while (!nodes.empty()){
		pNode = nodes.top();
		std::cout << pNode->m_nValue << "\t";
		nodes.pop();
	}
	cout << endl;
}

void PrintListReverse_Recursively(ListNode* pHead){
	ListNode* pNode = pHead;
	
	if ( pHead != NULL ){
		if (pNode->m_nNext != NULL) {
			PrintListReverse_Recursively(pNode->m_nNext);
		}
		std::cout << pNode->m_nValue << "\t";
	}
}

int main(){

	ListNode* pHead=NULL;


	insertListNode(&pHead, 4);
	insertListNode(&pHead, 8);
	insertListNode(&pHead, 6);
	insertListNode(&pHead, 1);
	insertListNode(&pHead, 2);
	insertListNode(&pHead, 0);

	PrintList(pHead);

	PrintListReverse_Iteratively(pHead);
	
	freeListNodeAll(&pHead);

	return 0;
}
