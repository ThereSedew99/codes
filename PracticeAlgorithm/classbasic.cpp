#include <iostream>
#include <cstring>
using namespace std;

template <class T>
class myClass{

	private:
		T a;
		char *name;

	public:
		myClass(){ }
		myClass(const T &_a, const char *_name){
			a = _a;
			name = new char[strlen(_name)+1];	
			strcpy(name, _name);
		}
#if 1
		myClass(const myClass &_T){
			a = _T.a;
			name = new char[strlen(_T.name)+1];
			strcpy(name, _T.name);

		}
#endif
		~myClass(){
			delete []name;
		}

		void showData(){
			cout << a <<endl;
			cout << name << endl;
		}

};

int main(int argc, char *argv[]){
	myClass<int> A(4,"spider man");
	myClass<int> B(A);

	B.showData();
}


