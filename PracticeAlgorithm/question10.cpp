#include <iostream>
#include <cstring>

#define MAXSTRLEN 100

using namespace std;

int concatenate(int *array1, int cnt1, int *array2, int cnt2){

	int index1=0, index2=0, targetIndex=0;

	if ( array1==NULL || array2==NULL ) return 0;	
	if ( cnt1<0 || cnt2<0 ) return 0;

	index1 = cnt1-1;
	index2 = cnt2-1;

	targetIndex = cnt1+cnt2-1;

	while ( index1>=0 || index2>=0 ){

		if ( index2<0 || ( *(array1+index1) > *(array2+index2) ) ) {
			*(array1+targetIndex) = *(array1+index1);
			index1--;
		} else {
			*(array1+targetIndex) = *(array2+index2);
			index2--;

		}

		targetIndex--;
	}

	return cnt1+cnt2;
}

int printArray(int *array, int cnt){
	int index=0;

	if ( array == NULL ) return -1;

	while (index<cnt){
		cout << *(array+index) << " ";
		index++;
	}
	cout << endl;

	return 1;
}

int main(){
	int cnt1 = 6;
	int array1[MAXSTRLEN*3] = {1,3,5,7,9,11};
	//int cnt2 = 6;
	//int array2[MAXSTRLEN] = { 2,4,6,8,10,12};
	int cnt2 = 2;
	int array2[MAXSTRLEN] = { 2,12};
	int mergedCnt=0;

	printArray(array1, cnt1);
	printArray(array2, cnt2);

	mergedCnt = concatenate(array1, cnt1, array2, cnt2);

	printArray(array1,mergedCnt);
}
