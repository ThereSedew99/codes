#include <iostream>

using namespace std;

bool scanNumber(char **str){

	if ( **str>='0' && **str<='9' ) {
		while ( **str>='0' && **str<='9' ) (*str)++;
		return true;
	} else {
		return false;
	}
}

bool isNumberKJ(char *str){
	if ( str==NULL && *str=='\0' ) return false;

	// check sign
	if ( *str=='+' || *str=='-' ) str++;		//state 0
	if ( str==NULL && *str=='\0' ) return false;

	// a int and skip next ints
	scanNumber(&str);				//state 5

	if ( *str == '.' ) {
		str++;
		if ( *str == '\0' ) return true;	//state 1

		// a int and skip next ints
		if (!scanNumber(&str)) return false;	

	}

	if ( *str == '\0' ) return true;		//state 2

	if ( *str=='e' || *str=='E' ) {			//state 3
		str++;

		// check sign
		if ( *str=='+' || *str=='-' ) str++;	//state 4
		if ( str==NULL && *str=='\0' ) return false;

		// a int and skip next ints
		if (!scanNumber(&str)) return false;

		if ( *str=='\0' ) return true;
	}

	return false;
}

int main(){

	char strArray[] = "-1E-16";
	//char strArray[] = "1.2.3";
	//char strArray[] = "12e+5.4";
	//char strArray[] = "1a3.14";
	//char strArray[] = "+100.";

	cout << "[" << strArray <<"] result " << isNumberKJ(strArray) << "[" << strArray << "]" << endl;

	return 0;
}
