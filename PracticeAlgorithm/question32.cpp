#include <iostream>

using namespace std;

int getLen(char *pStr){
	char * pTemp = NULL;
	int cnt = 0;

	pTemp = pStr;

	while (pTemp!=NULL && *pTemp!='\0'){
		cnt++;
		pTemp = pTemp+1;
	}

	return cnt;
}
int Min(int a, int b, int c){
	int minTemp = 0;

	minTemp = ( a < b ) ? a : b;
	minTemp = ( c < minTemp ) ? c : minTemp;

	return minTemp;
}
int getEditDistance(char *fromStr, char *toStr){
	int retDistance = -1;
	int lengthFromStr = getLen(fromStr);
	int lengthToStr = getLen(toStr);	

	if (fromStr==NULL || toStr==NULL) return -1;

	int *distanceMap = new int[(lengthFromStr+1)*(lengthToStr+1)]();
	int distanceCol = lengthFromStr+1;

	for (int i=0;i<=lengthFromStr;i++) *(distanceMap+0*distanceCol+i) = i;
	for (int j=0;j<=lengthToStr;j++) *(distanceMap+j*distanceCol+0) = j;

	for (int y=1;y<=lengthToStr;y++){
		for (int x=1;x<=lengthFromStr;x++){
			int prevX = x-1;
			int prevY = y-1;

			//cout <<x<<":"<<*(fromStr+x-1) << " - " <<y<<":"<< *(toStr+y-1); 
			if ( *(fromStr+x-1) == *(toStr+y-1) ) {
				//cout << " same";
				*(distanceMap+y*distanceCol+x) = *(distanceMap+prevY*distanceCol+prevX);
			} else {
				int deletion = *(distanceMap+prevY*distanceCol+x)+1;
				int insertion = *(distanceMap+y*distanceCol+prevX)+1;
				int replace = *(distanceMap+prevY*distanceCol+prevX)+1;
				//cout << deletion << "/" << insertion << "/" << replace ;
				*(distanceMap+y*distanceCol+x) = Min(deletion, insertion, replace);
			}
			//cout << endl;
		}
	}	

	for ( int y=0;y<=lengthToStr;y++){
		for ( int x=0;x<=lengthFromStr;x++){
			cout << *(distanceMap+y*distanceCol+x) <<"\t";
		}
		cout << endl;
	}

	retDistance = *(distanceMap+(lengthToStr)*distanceCol+(lengthFromStr));

	delete[] distanceMap;

	return retDistance;
}

int main(){

	char String1[] = "Saturday";
	char String2[] = "Sunday";

	cout << getEditDistance(String1, String2) << endl;

	return 0;
}
