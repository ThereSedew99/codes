#include <iostream>

using namespace std;

int isBalancedCore(BinaryTreeNode *pNode);

bool isBalanced(BinaryTreeNode *pNode){

	if ( pNode == NULL ) return false;

	if ( isBalancedCore(pNode) >=0 ) return true;

	return false;
}

int isBalancedCore(BinaryTreeNode *pNode){
	int leftLen=0, rightLen=0;

	if ( pNode == NULL ) return 0;

	leftLen = isBalancedCore(pNode->left);
	rightLen = isBalancedCore(pNode->right);

	if ( leftLen!=-1 && rightLen!=-1){
		if ( (leftLen-rightLen)=<1 && (leftLen-rightLen)>=-1 ) return (leftLen > rightLen)?leftLen+1:rightLen+1;

	}

	return -1;
}

int main(){

	isBalanced(NULL);

	return 0;
}
