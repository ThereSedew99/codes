#include <iostream>

using namespace std;


//////////////////////////////////////////
class solution2_sum{

public:
	solution2_sum() { ++N; Sum+=N; }

	static void Reset() { N=0; Sum=0; }
	unsigned int GetSum() { return Sum; }

private :
	static unsigned int N;
	static unsigned int Sum;
};

unsigned int solution2_sum::N = 0;
unsigned int solution2_sum::Sum = 0;


//////////////////////////////////////////
typedef unsigned int (*fun)(unsigned int);
unsigned int solution3_terminator(unsigned int n){
	return 0;
}

unsigned int solution3_sum(unsigned int n){
	//unsigned int (*f[2])(unsigned int) = {solution3_terminator, solution3_sum };
	unsigned int (*f[2])(unsigned int) = {solution3_terminator, solution3_sum };
	return n + f[!!n](n-1);
	
}


//////////////////////////////////////////
template <unsigned int n> 
struct solution4_sum {
	enum Value { N = solution4_sum<n-1>::N + n };
};

template <> 
struct solution4_sum<1> {
	enum Value { N = 1 };
};

int main(){

	
	cout << solution3_sum(3) << endl;

	solution4_sum<3> solution;
	cout << solution.N << endl;

	solution2_sum::Reset();	
	solution2_sum *a = new solution2_sum[3];
	cout << a[0].GetSum() << endl;
	delete[] a;
	a = NULL;


	return 0;

}
