#include <iostream>
#include <bitset>

using namespace std;

int countOneFlag(int input){
	unsigned int flag = 1;
	int cnt = 0;

	if (input==0) return 0;

	while ( flag ) {
		if ( input & flag ) cnt++;
		flag = flag << 1;	
	}

	return cnt;
}

int countOneBit(int input){

	int temp = input;
	int cnt = 0;

	while ( temp ) {	
		temp = (temp-1)&temp;
		cnt++;
	}

	return cnt;
}

bool isPower2(int input){
	if ( (input!=0) && (((input-1)&input) == 0) ) return true;
	else return false;
}

int main(){

	int in = 149;
	cout << bitset<sizeof(in)*8>(in) << endl;
	cout << countOneFlag(in) << endl;
	cout << countOneBit(in) << endl;
	cout << bitset<sizeof(in)*8>(256) <<" : " << isPower2(256) << endl;
	return 0;
}
