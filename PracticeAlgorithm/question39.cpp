#include <iostream>
#include <bitset>
#include <cmath>

using namespace std;

int getOnlyOnce(int* numbers, int size, int *ret){
	int totalXOR = 0;

	if ( size <=0 ) return 0;

	*(ret+0) = 0;
	*(ret+1) = 0;

	for ( int i=0; i<size; i++){
		cout << *(numbers+i) << " : " << bitset<16>(*(numbers+i)) << endl;
		totalXOR ^= *(numbers+i);
	}

	cout << bitset<32>(totalXOR) << endl;

	for ( int i=0; i<size; i++){
		if ( totalXOR & *(numbers+i) ){
			*(ret+0) ^= *(numbers+i);

		} else {
			*(ret+1) ^= *(numbers+i);

		}
	}

}

int getMissingNum(int* numbers, int size, int *ret) {
	int fullLen = size+2;
	int backNumbers[size+fullLen];

	if ( size <=0 ) return 0;

	for (int i=0;i<size;i++){
		backNumbers[i] = *(numbers+i);
	}
	for (int i=1;i<=fullLen;i++){
		backNumbers[i+size-1] = i;
	}

	getOnlyOnce(backNumbers, size+fullLen, ret);	

	return 1;
}

int getMissingNumCal(int* numbers, int size, int *ret){

	int sum1=0, sum2=0;
	int prod1=1, prod2=1;

	if ( size <=0 ) return 0;

	for (int i=0; i<size; i++){
		sum1  += *(numbers+i);
		prod1 *= *(numbers+i);
	}

	for ( int i=1 ; i<=(size+2) ; i++) {
		sum2 += i;
		prod2 *= i;
	}

	sum1 = sum2-sum1;
	prod1 = prod2/prod1;


	*(ret+0) = (sum1 + (int)sqrt(sum1*sum1 - 4*prod1)) / 2;
	*(ret+1) = (sum1 - (int)sqrt(sum1*sum1 - 4*prod1)) / 2;

	return 1;
}


int main(){
	int numbers[] = { 0,1,2,4,4,3,6,3,2,5,5 };
	int Once[2];
	int size = sizeof(numbers)/sizeof(int);

	getOnlyOnce(numbers, size, Once);
	cout << Once[0] << ", " << Once[1] << endl;


	int numbers2[] = { 1,2,4,5,6,8,9,10,11 };
	size = sizeof(numbers2)/sizeof(int);
	getMissingNum(numbers2, size, Once);
	cout << Once[0] << ", " << Once[1] << endl;

	getMissingNumCal(numbers2, size, Once);
	cout << Once[0] << ", " << Once[1] << endl;

 }
