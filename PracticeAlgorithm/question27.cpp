#include <iostream>
#include <new>
using namespace std;

class IntArray {

private:
	int* numbers = NULL;

public:
	int size;
	int len;

	IntArray(int val){
		numbers = new (nothrow) int[val];
		size = val;
		len = 0;
	}

	~IntArray(){
		size = 0;
		delete numbers;
	}
	
	int& operator [] (int x){
		return numbers[x];
	}

	IntArray& operator = (const IntArray& param){
		cout << "Overload for class assign\n";
	}
	
	int getMin_circular();

};

int IntArray::getMin_circular(){
	int startP = 0;
	int endP = 0;
	int midP = 0;

	if (len <=0) return -1;

	//check circular

	startP = 0;
	endP = len-1;

	while(1){

		if ( startP >= endP ) break;

		midP = (endP + startP)/2;
		if ( midP < 0 ) midP = 0;
		if ( startP == midP ) break;

		cout << startP <<" ~ "<< midP << " ~ " << endP <<"\n";

		if ( numbers[midP] > numbers[startP] )
		{
			startP = midP;
		} else if (numbers[midP] < numbers[startP] ) {
			endP = midP;
		} else {
			//seq search
			while(1){
				if ( startP == midP ) break;

				if ( numbers[startP] <= numbers[midP] ){
					startP = startP+1;
					continue;
				}
			}
		}
	}

	return numbers[startP]; 
}

int main(){

	IntArray inArray(10);
	int minVal = -1;

	inArray[0] = 3;
	inArray[1] = 4;
	inArray[2] = 5;
	inArray[3] = 1;
	inArray[4] = 2;
	inArray.len = 5;

	for (int i=0;i<10;i++){
		cout << "[" << i << "] " << inArray[i];
	}
	cout << "\n";

	cout << "Circular min :" << inArray.getMin_circular() <<"\n";

	return 0;
}
