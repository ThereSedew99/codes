#include <iostream>
#include <bitset>
using namespace std;

int getOnlyOnce(int* numbers, int size, int *ret){
	int totalXOR = 0;

	*(ret+0) = 0;
	*(ret+1) = 0;

	for ( int i=0; i<size; i++){
		cout << bitset<16>(*(numbers+i)) << endl;
		totalXOR ^= *(numbers+i);
	}

	cout << bitset<32>(totalXOR) << endl;

	for ( int i=0; i<size; i++){
		if ( totalXOR & *(numbers+i) ){
			*(ret+0) ^= *(numbers+i);

		} else {
			*(ret+1) ^= *(numbers+i);

		}
	}

}

int main(){
	int numbers[] = { 0,1,2,4,4,3,6,3,2,5,5 };
	int Once[2];
	int size = sizeof(numbers)/sizeof(int);

	getOnlyOnce(numbers, size, Once);
	cout << Once[0] << ", " << Once[1] << endl;
 }
