#include <iostream>

using namespace std;

int max(int leftDepth, int rightDepth){
	if ( leftDepth <= rightDepth ) return rightDepth;
	else return leftDepth;
}

int getDepth(BinaryTreeNode *pNode){
	unsigned int depth=0;

	if (pNode==NULL) return 0;

	return max(getDepth(pNode->left), getDepth(pNode->right))+1;
}

int main(){




}
