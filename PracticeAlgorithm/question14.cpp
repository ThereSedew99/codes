#include <iostream>

using namespace std;

struct ListNode {
	ListNode* m_nNext;
	int m_nValue;
};

int insertListNode(ListNode** pHead, int value) {
	ListNode *tempNode=NULL;

	//tempNode = (ListNode*)malloc(sizeof(ListNode));
	tempNode = new ListNode;

	if (tempNode==NULL) return 0;

	tempNode->m_nValue = value;
	tempNode->m_nNext = NULL;

	if ( *pHead == NULL ) {
		*pHead = tempNode;
	} else {
		tempNode->m_nNext = *pHead;	
		*pHead = tempNode;
	}

	return 1;
}

int freeListNodeAll(ListNode** pHead){
	ListNode *tempNode=NULL;

	while (*pHead!=NULL){
		tempNode = *pHead;
		*pHead = (*pHead)->m_nNext;
		delete tempNode;
	}

	return 0;
}

int PrintList(ListNode* pHead){
	while ( pHead!=NULL ){

		cout << pHead->m_nValue << "\t" ;
		pHead = pHead->m_nNext;
	}
	cout << endl;

	return 1;
}


//to change the pHead content, it should be used with double pointer
int sortList(ListNode **pHead){
	ListNode* sortedEnd = NULL;	
	ListNode* nextNode  = NULL;	
	ListNode* tempNode  = NULL;

	if (pHead == NULL && *pHead == NULL) return 0;

	cout << "pHead Address :" << pHead << endl;
	cout << "&pHead Value :" << &pHead << endl;
	cout << "*pHead Address :" << *pHead << endl;

	sortedEnd = *pHead;
	tempNode = *pHead;
	
	while ( sortedEnd !=NULL ){
		nextNode = sortedEnd->m_nNext;
		if (nextNode == NULL ) break;

		if ( sortedEnd->m_nValue > nextNode->m_nValue ){ 
			//find a position from previous sorted Node list for NextNode
			sortedEnd->m_nNext = nextNode->m_nNext;

			if ( nextNode->m_nValue < (*pHead)->m_nValue ){
				nextNode->m_nNext = *pHead;
				*pHead = nextNode;
				continue;
			}

			for ( tempNode = *pHead ; tempNode != sortedEnd ; tempNode = tempNode->m_nNext ){

				if ( tempNode->m_nNext->m_nValue > nextNode->m_nValue ){
					nextNode->m_nNext = tempNode->m_nNext;
					tempNode->m_nNext = nextNode;
					break;
				}	
			}

		} else {
			sortedEnd = nextNode;
		}

		
	}

	return 1;
}


int main(){

	ListNode* pHead=NULL;


	insertListNode(&pHead, 4);
	insertListNode(&pHead, 8);
	insertListNode(&pHead, 6);
	insertListNode(&pHead, 1);
	insertListNode(&pHead, 2);
	insertListNode(&pHead, 0);
	insertListNode(&pHead, 9);

	

	PrintList(pHead);

	
	cout << "pHead Address :" << pHead << endl;
	cout << "&pHead Address :" << &pHead << endl;

	sortList(&pHead);

	cout << "pHead Address :" << &pHead << endl;
	PrintList(pHead);

	
	freeListNodeAll(&pHead);

	return 0;
}
