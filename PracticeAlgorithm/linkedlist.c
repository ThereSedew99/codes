#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct node {
	int value;
	node *next;
	node *before;
	node *mother;
} Node;

Node* insertNodeToHead(Node** head, int value){
	Node* nodePoint=0;

	nodePoint = (Node *)malloc(sizeof(Node));
	nodePoint->next = 0;
	nodePoint->before = 0;
	nodePoint->mother = 0;
	nodePoint->value = value;

	if ( *head ){
		nodePoint->next = *head;
		nodePoint->before = (*head)->before;
		(*head)->before = nodePoint;
		*head = nodePoint;
	} else {
		*head = nodePoint;
		(*head)->before = nodePoint;
	}

	return nodePoint;
}

Node* insertNodeToEnd(Node** head, int value){
	Node* nodePoint=0;

	nodePoint = (Node *)malloc(sizeof(Node));
	nodePoint->next = 0;
	nodePoint->before = 0;
	nodePoint->mother = 0;
	nodePoint->value = value;

	if ( *head == 0 ){
		*head = nodePoint;
		printf("head null + [%x]\n",nodePoint);
	} else {
		if ( (*head)->before != 0 ) {
			(*head)->before->next = nodePoint;
		} else {
			(*head)->next = nodePoint;
		}
		nodePoint->before = (*head)->before;	
		(*head)->before = nodePoint;
	}

	return nodePoint;
}

int freeAllNode(Node** head){
	Node* targetToDelete = 0;

	while (*head){
		targetToDelete = (*head)->next;
		if ( targetToDelete ){
			(*head)->next = targetToDelete->next;
			free(targetToDelete);		
			targetToDelete = 0;
			(*head)->next = 0;
		} else {
			free(*head);
			*head=0;
			break;
		}
	}

	return 0;
}

Node* makeBinaryTree(Node** head){
	Node* treeHead=0;

	return treeHead;
}

int printNodes(Node* head){
	Node* nodePointer=0;

	nodePointer=head;
	while (nodePointer){
		printf("-[%d][%x]", nodePointer->value, nodePointer);
		nodePointer = nodePointer->next;
	}	
	printf("\n");
}

int main(){
	Node * head = 0;
	Node * tail = 0;
	Node * treeHead = 0;
	int ret=0;

	tail = insertNodeToEnd(&head,3);
	tail = insertNodeToEnd(&head,5);
	tail = insertNodeToEnd(&head,1);
	tail = insertNodeToHead(&head,2);

	printNodes(head);

	//make a binary tree
	treeHead = makeBinaryTree(&head);
	
	ret = freeAllNode(&head);
	
	printNodes(head);

	return -1;
}
