#include <stdio.h>
#include <string.h>

#define MAXSTRING 200
#define MAXPATTERN 50

// needs to check correct hashKey
#define hashKey 101	
#define DEBUG //printf
#define radix 256 

int getLen(const char* StringArray){
	int i=0;
	const char* temp=StringArray;

	while (*temp){
	 	i++;
		temp = temp+1;
	}
	return i;
}

int hash(const char* StringArray, int ArrayLength) {
	int hashResult=0;
	int index=0;
	const char* temp=StringArray;

	for (index=0;index<ArrayLength; index++){
		hashResult = ( hashResult*radix + *(temp+index) ) % hashKey;
	}

	return hashResult;
}

int hashFingerPrint(const char* StringArray, int startIdx, int ArrayLength, int prevHash, int dm) {
        int hashResult=0;
	int result = 0;
        int index=0;
        const char* temp=StringArray+startIdx;

	if ( getLen( (temp) ) < ArrayLength ) return -1;

	DEBUG("ArrayLen[%d] dm[0x%x] startIdx[%d] ",ArrayLength, dm, startIdx);

	if ( startIdx <= 0 ) {
		// First window hash calculation
		hashResult = hash(StringArray, ArrayLength);
	}
	else {
		// calculate the hasing using previous window calculation
		// WRONG CALCULATION - NEEDS TO FIX
		hashResult = ((prevHash - ((*(temp-1))*dm)%hashKey + hashKey)*radix)%hashKey + *(temp+(ArrayLength-1)%hashKey);
		hashResult = hashResult % hashKey;
		if ( hashResult < 0 ) hashResult = hashResult + radix;

		DEBUG("prevHash[%x] preChar[%c/%x] lastChar[%c/%x]", prevHash, *(temp-1),*(temp-1), *(temp+(ArrayLength-1)),*(temp+(ArrayLength-1)) );
		DEBUG("expectedKey[%x] ", hash(temp, ArrayLength));
	}	
        DEBUG("Resultkey [%x]\n", hashResult);
        return hashResult;
}

int nativeCompare(const char* needle, const char* haystack){
	int haystackLen=getLen(haystack);
	int needleLen=getLen(needle);
	int shortLen = haystackLen<needleLen ? haystackLen : needleLen;
	int i=0;


	for (i=0; i<shortLen;i++){
		if ( *(haystack+i) == *(needle+i) ) continue;
		else break;
	}

	DEBUG("[%s] [%s] len[%d] ind[%d]\n", needle, haystack,shortLen,i);	

	if (i==shortLen) return 0;
	else return -1;
}

int rabinKarpMatch(const char* needle,const char* haystack){
	int index=0;
	int needleLength = getLen(needle);
	int haystackLength = getLen(haystack);
	int hashKeyNeedle = 0;
	int resultFlag = 0;
	int hashPrevFingerPrint = 0;
	int dm = 1;

	printf("needle[%s] haystack[%s] radix[%x] hashKey[%d]\n", needle, haystack, radix, hashKey);	

	if ( needleLength > haystackLength ) return -1;
	if ( *needle == 0 | *haystack == 0 ) return -1;
	
	hashKeyNeedle = hash(needle, needleLength);

	//pre-compute
	for (int i=0;i<needleLength-1;i++){
		dm = (radix*dm) % hashKey;
	}

	//hash key search for needle
	for (index=0;index<=haystackLength-needleLength;index++){

		hashPrevFingerPrint = hashFingerPrint(haystack, index, needleLength, hashPrevFingerPrint, dm);

		// collision cehck
		if ( hashKeyNeedle == hashPrevFingerPrint){
			if ( nativeCompare(needle,haystack+index)==0 ){
				resultFlag=1;
				printf("- Found at index[%d]\n", index);
				//break;
			}
		}
	}

	if ( resultFlag == 0 ) return -1;
	return index;
} 

int main(){
	
	rabinKarpMatch("GEEK", "GEEK for GEEKs");

	return 1;
}
