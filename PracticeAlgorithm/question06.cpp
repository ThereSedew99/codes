#include <iostream>

using namespace std;

int duplicated(int *InArray, int numbers){
	int valArray = 0;
	int temp=0;
	int ret=0;

	if (InArray==NULL) return -1;

	for (int i=0; i<numbers; ){

		if ( InArray+i == NULL ) return -1;

		valArray = *(InArray+i);
		if ( valArray < 0 || valArray >= numbers) return -1;

		if  ( valArray==i ) i++;
		else {
	
			if ( valArray == *(InArray+valArray) ){
				cout << "Duplicated " << valArray << endl;
				i++;
				ret = 1;
			} else {
				*(InArray+i) = *(InArray+valArray);
				*(InArray+valArray) = valArray;
			}
		}
	}

	return ret;
}

int main(){
	// check available examples
	int input[] = {2,3,1,0,2,5,3};
	//int input[] = {1,2,1,2,1,2,1};
	//int input[] = {0,0};
	//int input[] = {0,-1};
	//int input[] = {7,6,5,4,3,2,1};
	//int input[] = {6,5,4,3,2,1,0};
	//int input[] = {0,1,2,3,4,5,6};
	int length = sizeof(input)/sizeof(int);

	cout << "Duplicated result : " << duplicated(input, length) << endl;
	
	return 0;
}
