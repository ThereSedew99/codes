#include <iostream>

#define rowNum 4
#define colNum 4

using namespace std;

int searchMatrixSub(int *pMatrix, int rowCnt, int colCnt, int value, int startRow, int endRow, int startCol, int endCol);
int simpleMatrixSearch(int *pMatrix, int rowCnt, int colCnt, int value);

//Find a number in a matrix which has been sorted in row and col
int searchMatrix(int *pMatrix,int rowCnt, int colCnt, int value){
	int row=0;
	int start=0,mid=0,end=0;
	int midValue=0;

	if ( pMatrix == NULL ) return -1;
	if ( rowCnt<=0 || colCnt<=0 ) return -1;

	cout << "value " << value << endl;

	//return searchMatrixSub(pMatrix, rowCnt, colCnt, value, 0, rowCnt-1, 0, colCnt-1);
	return simpleMatrixSearch(pMatrix, rowCnt, colCnt, value);

}

int simpleMatrixSearch(int *pMatrix, int rowCnt, int colCnt, int value){

	int colIndex = colCnt-1;
	int rowIndex = 0;
	int curValue = 0;

	while ( colIndex >= 0 && rowIndex < rowCnt ){

		curValue = *(pMatrix+rowIndex*colCnt+colIndex);

		if ( curValue == value ) return 1;
		else if ( curValue < value ) rowIndex++;
		else if ( curValue > value ) colIndex--;	
		
	}

	return 0;
}

int searchMatrixSub(int *pMatrix, int rowCnt, int colCnt, int value, int startRow, int endRow, int startCol, int endCol){

	int midRow=0, midCol=0;
	int midValue=0;

	int startRowOri = startRow;
	int startColOri = startCol;
	int endRowOri = endRow;
	int endColOri = endCol;

	if ( pMatrix==NULL ) return 0;
	if ( rowCnt<=0 && colCnt<=0 ) return 0;

	int startValue = *(pMatrix+startRow*colCnt+startCol);
	int endValue = *(pMatrix+endRow*colCnt+endCol);

	if ( value == startValue || value == endValue ) return 1;

	//cout << " range " << startRow << "," << startCol << " - " << endRow << "," << endCol << endl; 

	// Search Diagonal with binary search for less than the target value
	while ( startRow<=endRow && startCol<=endCol ){
		//cout << "start " << startRow << "," << startCol ;
		//cout << " end " << endRow << "," << endCol << endl;

		midRow = (startRow+endRow)/2;
		midCol = (startCol+endCol)/2;
		midValue = *(pMatrix+midRow*colCnt+midCol);

		if ( midValue == value ) return 1;
		else if ( midValue > value ) {
			endRow=midRow-1;
			endCol=midCol-1;
		}
		else {
			startRow = midRow+1;
			startCol = midCol+1;
		}

	}
	
	startValue = *(pMatrix+startRow*colCnt+startCol);
	//cout << "lowest big " << startRow << ", " << startCol << " : " << startValue << endl;

	// get partition 1
	if ( startRow-1 < startRowOri ) return 0;
	if (searchMatrixSub(pMatrix, rowCnt, colCnt, value, startRowOri, startRow-1, startCol, endColOri)) return 1;
	
	// get partition 2
	if ( startCol-1 < startRowOri ) return 0;
	if (searchMatrixSub(pMatrix, rowCnt, colCnt, value, startRow, endRowOri, startColOri, startCol-1)) return 1;	


	return 0;
}

int main(){
	int aMatrix[rowNum][colNum] = { {1,2,8,9},{2,4,9,12},{4,7,10,13},{6,8,11,15} };

	cout << "return :" << searchMatrix((int*)aMatrix, rowNum, colNum, 12) << endl<<endl;
	cout << "return :" << searchMatrix((int*)aMatrix, rowNum, colNum, 8) << endl<<endl;
	cout << "return :" << searchMatrix((int*)aMatrix, rowNum, colNum, 15) << endl<<endl;
	cout << "return :" << searchMatrix((int*)aMatrix, rowNum, colNum, 16) << endl<<endl;
	cout << "return :" << searchMatrix((int*)aMatrix, rowNum, colNum, 0) << endl<<endl;
	cout << "return :" << searchMatrix((int*)aMatrix, rowNum, colNum, 1) << endl<<endl;
	cout << "return :" << searchMatrix((int*)aMatrix, rowNum, colNum, 6) << endl<<endl;

	return 0;
}
