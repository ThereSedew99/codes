#include <iostream>

using namespace std;

int swap (int* array, int source, int target){

	int temp = *(array+source);
	*(array+source) = *(array+target);
	*(array+target) = temp;

	return 1;
}

int partition(int* array, int start, int end){
	int pivot=0;
	int LargeStart=0;

	if ( end < start ) return 0;
	if ( start == end ) return 1;

	pivot = (start+end+1)/2; // or random value
	swap(array, pivot, end);

	LargeStart = start;

	for ( int i=start; i<end; i++ ){
		if ( *(array+i) < *(array+end) ){
			if ( i!= LargeStart ) {
				swap(array,LargeStart, i);
			}
			LargeStart++;
		}
	}

	if ( LargeStart != end ) swap(array, LargeStart, end);
	return LargeStart;	
}

int checkMaj(int *array, int size, int val){
	int cnt=0;
	for (int i=0;i<size-1;i++){
		if ( val == *(array+i) ) cnt++;
	}

	if ( cnt > (size/2) ) return 1;
	else return  0;
}

int findMaj(int *array, int size, int *ret){
	int pivot=0;
	int start=0;
	int end = size-1;
	int mid = end/2; 

	if ( size== 0 ) return 0;

	while ( pivot != mid ){
		pivot = partition(array,start,end);

		if ( pivot > mid ) end = pivot-1;
		if ( pivot < mid ) start = pivot+1;
	}

	*ret = *(array+mid);

	return checkMaj(array, size, (int)*ret);

}

int main(){
	int array[] = { 1,2,3,2,2,2,5,4,2 };
	int size = sizeof(array)/sizeof(int);
	int pivot = 0;
	int maj = 0;
	
	for (int i=0;i<size;i++) cout << array[i] ;
	cout << endl;
	

	if (findMaj(array, size, &maj)) cout << maj << endl;
	else cout << "No maj" << endl;
	return 0;
}
