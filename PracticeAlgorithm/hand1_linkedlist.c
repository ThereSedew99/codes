#include <stdio.h>
#include <stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
} aNode ;

aNode* insertNodeEnd(aNode **pHead, int data);
int freeall(aNode **pHead);
int printList(aNode *head);

int main(){
	aNode* headNode = NULL;

        insertNodeEnd(&headNode, 3);
	insertNodeEnd(&headNode, 4);
	insertNodeEnd(&headNode, 6);

	printList(headNode);

	freeall(&headNode);
}

int printList(aNode *head){
	aNode *tempNode = head;

	while (tempNode!=NULL){
		printf("%d \t",tempNode->data);
		tempNode = tempNode->next;
	}

	return 0;
}

int freeall(aNode **pHead){
	aNode *tempNode = NULL;

	while (*pHead!=NULL){
		tempNode = (*pHead)->next;
		free(*pHead);
		*pHead = tempNode;
	}

	return 0;
}

aNode* insertNodeEnd(aNode **pHead, int data){
	aNode *currentNode = *pHead;
	aNode *newNode = (aNode *)malloc(sizeof(aNode));

	if (newNode==NULL) return NULL;
	newNode->next = NULL;
	newNode->data = data;

	if(*pHead==NULL) { 
		*pHead = newNode; 
	}
	else {
		while (currentNode->next != NULL){
			currentNode = currentNode->next;
		}
		currentNode->next = newNode;
	}

	return currentNode;	
}
