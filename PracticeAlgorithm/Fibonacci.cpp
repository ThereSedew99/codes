#include <iostream>
using namespace std;

long long Fibonacci_recur(unsigned int n){
	if (n<=0) return 0;
	if (n==1) return 1;

	return Fibonacci_recur(n-1) + Fibonacci_recur(n-2);
}

long long Fibonacci_iter(unsigned int n){
	int i=0;
	long long Fn_2=0;
	long long Fn_1=1;
	long long Fn = 0;

	if (n<=0) return Fn_2;
	if (n==1) return Fn_1;

	for (i=2;i<=n;i++){
		Fn = Fn_1 + Fn_2;	
		Fn_2 = Fn_1;
		Fn_1 = Fn;
	}

	return Fn;
}
// enahnce this function to O(n) Time with Iterative

int main(){
	long long ret_1=0, ret_2=0;

	ret_1 = Fibonacci_recur(10);
	ret_2 = Fibonacci_iter(10);

	cout << ret_1;
	cout << ret_2;
	return 0;
}
