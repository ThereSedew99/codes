#include <iostream>
#include <stack>
#include <cstring>

using namespace std;

struct ListNode {
	ListNode* m_nNext;
	int m_nValue;
};

int insertListNode(ListNode** pHead, int value) {
	ListNode *tempNode=NULL;

	//tempNode = (ListNode*)malloc(sizeof(ListNode));
	tempNode = new ListNode;

	if (tempNode==NULL) return 0;

	tempNode->m_nValue = value;
	tempNode->m_nNext = NULL;

	if ( *pHead == NULL ) {
		*pHead = tempNode;
	} else {
		tempNode->m_nNext = *pHead;	
		*pHead = tempNode;
	}

	return 1;
}

int freeListNodeAll(ListNode** pHead){
	ListNode *tempNode=NULL;

	while (*pHead!=NULL){
		tempNode = *pHead;
		*pHead = (*pHead)->m_nNext;
		delete tempNode;
	}

	return 0;
}

int PrintList(ListNode* pHead){
	while ( pHead!=NULL ){

		cout << pHead->m_nValue << "\t" ;
		pHead = pHead->m_nNext;
	}
	cout << endl;

	return 1;
}


//to change the pHead content, it should be used with double pointer
int sortList(ListNode **pHead){
	ListNode* sortedEnd = NULL;	
	ListNode* nextNode  = NULL;	
	ListNode* tempNode  = NULL;

	if (pHead == NULL && *pHead == NULL) return 0;

	sortedEnd = *pHead;
	tempNode = *pHead;
	
	while ( sortedEnd !=NULL ){
		nextNode = sortedEnd->m_nNext;
		if (nextNode == NULL ) break;

		if ( sortedEnd->m_nValue > nextNode->m_nValue ){ 
			//find a position from previous sorted Node list for NextNode
			sortedEnd->m_nNext = nextNode->m_nNext;

			if ( nextNode->m_nValue < (*pHead)->m_nValue ){
				nextNode->m_nNext = *pHead;
				*pHead = nextNode;
				continue;
			}

			for ( tempNode = *pHead ; tempNode != sortedEnd ; tempNode = tempNode->m_nNext ){

				if ( tempNode->m_nNext->m_nValue > nextNode->m_nValue ){
					nextNode->m_nNext = tempNode->m_nNext;
					tempNode->m_nNext = nextNode;
					break;
				}	
			}

		} else {
			sortedEnd = nextNode;
		}

		
	}

	return 1;
}

ListNode* mergeSortedList(ListNode* pHead1, ListNode* pHead2){

	ListNode *newHead = NULL;
	ListNode *tempNode = NULL;
	ListNode *targetNode = NULL;
	int shortcut=0;

	while ( pHead1!=NULL || pHead2!=NULL ){

		// enhance		
		if ( pHead1==NULL || pHead2==NULL ) shortcut=1;

		// select target
		if( pHead1==NULL || (pHead1->m_nValue >= pHead2->m_nValue) ){
			targetNode = pHead2;	
			pHead2 = pHead2->m_nNext;
		} else if ( pHead2==NULL || (pHead1->m_nValue < pHead2->m_nValue) ){
			targetNode = pHead1;
			pHead1 = pHead1->m_nNext;
		}

		// make new list
		if ( newHead == NULL ){
			newHead = targetNode;
			tempNode = newHead;
		} else {
			tempNode->m_nNext = targetNode;
			tempNode = targetNode;
		}

		//enhance
		if (shortcut==1) break;

	}

	return newHead;
}

int main(){

	ListNode* pHead1=NULL;
	ListNode* pHead2=NULL;
	ListNode* pHead3=NULL;

	insertListNode(&pHead1, 4);
	insertListNode(&pHead1, 8);
	insertListNode(&pHead1, 6);
	insertListNode(&pHead1, 1);
	insertListNode(&pHead1, 2);
	insertListNode(&pHead1, 0);
	insertListNode(&pHead1, 9);

	
	insertListNode(&pHead2, 3);
	insertListNode(&pHead2, 11);
	insertListNode(&pHead2, 12);
	insertListNode(&pHead2, 13);
	insertListNode(&pHead2, 0);
	insertListNode(&pHead2, 5);
	insertListNode(&pHead2, 7);

	PrintList(pHead1);
	PrintList(pHead2);
	

	sortList(&pHead1);
	sortList(&pHead2);

	PrintList(pHead1);
	PrintList(pHead2);

	pHead3 = mergeSortedList(pHead1, pHead2);	
	PrintList(pHead3);

	freeListNodeAll(&pHead3);

	return 0;
}
