#include <iostream>

using namespace std;

int findDuplicated(int *inArray, int length){

	int sumArray=0;
	int sumMaxSub = (length-2)*(length-1)/2;

	// error check
	if ( inArray == NULL ) return -1;

	for ( int i=0; i<length ;i++ ){
		// check the restriction
		if ( *(inArray+i) < 0 || *(inArray+i)>length-1 ) return -1;
		sumArray = sumArray + *(inArray+i);
	}

	return sumArray-sumMaxSub;
}

int main(){

	int input[] = { 0,2,1,3,4,3 }; // static size array
	int inputLen = sizeof(input)/sizeof(int);

	cout << "Duplicated : " << findDuplicated(input, inputLen) << endl;

}
