#include <iostream>

using namespace std;

class Node{
	int data;
	Node *next;

public:
	Node() { data = 0; next = NULL; }
	Node(int inData) { data = inData; next = NULL; }
	~Node() { }
	int getData() { return data; }
	void setData(int inData) { data = inData; }
	Node* getNext() { return next; }
	int setNext(Node *nextNode) { next = nextNode; }
};

class singleList {
	Node *head;
	
public :
	singleList() {
		head = NULL;
	}
	~singleList();

	int insertNodeEnd(int indata);
	void printNodeAll();

};

int singleList::insertNodeEnd(int inData){
	Node *newNode = new Node(inData);
	if ( head == NULL ) { head = newNode; }
	else {
		Node *tempNode = head;
		while ( tempNode->getNext() != NULL ){
			tempNode = tempNode->getNext();
		}
		tempNode->setNext(newNode);
	}
}

singleList::~singleList(){
	Node *tempNode = NULL;
	while ( head != NULL ){
		tempNode = head->getNext();
		delete(head);
		head = tempNode;
	}
}

void singleList::printNodeAll(){
	Node *tempNode = head;

	while ( tempNode != NULL ){
		cout << tempNode->getData() << "\t";
		tempNode = tempNode->getNext();
	}
}

int main(){
	singleList aList;

	aList.insertNodeEnd(3);
	aList.insertNodeEnd(4);
	aList.insertNodeEnd(5);
	aList.printNodeAll();

	return 0;
}
	
