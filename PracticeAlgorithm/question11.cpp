#include <iostream>

using namespace std;

int patternCheck(char *pStr, char *pPat){

	if ( pStr == NULL || pPat == NULL ) return 0;

	// return 1 when both pStr and pPat reaches to end
	if ( *pPat == '\0' && *pStr == '\0' ) return 1;

	//mispatch case
	if ( *pPat == '\0' && *pStr !='\0' ) return 0;

	// state transition
	if ( *(pPat+1) =='*' ) {
		if ( *pPat == *pStr || ( *pPat == '.'&& *pStr!='\0' ) )
			return patternCheck(pStr+1, pPat) || 
				patternCheck(pStr+1, pPat+2) || 
				patternCheck(pStr, pPat+2);
		else return patternCheck(pStr, pPat+2);
		
	} else if ( (*pPat == '.'&& *pStr!='\0') || (*pPat == *pStr) ) {
		return patternCheck(pStr+1, pPat+1);
	}

	return 0;
}

int main(){

	//char strArray[] = "";
	//char strPattern[] = ".*";

	//char strArray[] = "";
	//char strPattern[] = ".";

	char strArray[] = "aaa";
	char strPattern[] = "ab*ac*a";

	cout << "result " << patternCheck(strArray, strPattern) << endl;

	return 0;
}
