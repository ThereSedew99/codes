#include <iostream>
#include <stack>
#include <queue>
#include <stdexcept>

using namespace std;

template <typename T>
class QueueWithTwoStacks {
	stack<T> *stack1;
	stack<T> *stack2;
	
public :
		
	QueueWithTwoStacks();
	~QueueWithTwoStacks();
	void enqueue(T item);
	T dequeue();
};

template <typename T>
QueueWithTwoStacks<T>::QueueWithTwoStacks(){
	stack1 = new stack<T>;
	stack2 = new stack<T>;
}

template <typename T>
QueueWithTwoStacks<T>::~QueueWithTwoStacks(){
	delete stack1;
	delete stack2;
}

template <typename T>
void QueueWithTwoStacks<T>::enqueue(T item){
	stack1->push(item);
}

template <typename T>
T QueueWithTwoStacks<T>::dequeue(){
	T tempItem;

	if ( stack2->empty() ) {
		while ( !stack1->empty() ){
			tempItem = stack1->top();
			stack1->pop();
			stack2->push(tempItem);
		}
	}

	if ( stack2->empty() ) {
		throw underflow_error("Queue is empty"); 
	}

	tempItem = stack2->top();
	stack2->pop();

	return tempItem;
}

template <typename T>
class StackWithTwoQueues{
	queue<T> *queue1;
	queue<T> *queue2;

public :
	StackWithTwoQueues();
	~StackWithTwoQueues();
	T top();
	T pop();
	int push(T item);
};

template<typename T>
StackWithTwoQueues<T>::StackWithTwoQueues(){
	queue1 = new queue<T>;
	queue2 = new queue<T>;
}

template<typename T>
StackWithTwoQueues<T>::~StackWithTwoQueues(){
	delete queue1;
	delete queue2;
}

template<typename T>
T StackWithTwoQueues<T>::top(){

}


template<typename T>
T StackWithTwoQueues<T>::pop(){
	T tempItem;

	queue<T> *sourceQ = NULL;
	queue<T> *targetQ = NULL;

	if ( queue1->empty() && queue2->empty() ) throw underflow_error("Queue is empty");

	if ( !queue1->empty() ) {
		sourceQ = queue1; 
		targetQ = queue2;
	}
	else if ( !queue2->empty() ){
		sourceQ = queue2;
		targetQ = queue1;
	}

	while ( sourceQ->size() > 1 ) {
		targetQ->push(sourceQ->front());
		sourceQ->pop();
	}

	tempItem = sourceQ->front();
	sourceQ->pop();

	return tempItem;
}

template<typename T>
int StackWithTwoQueues<T>::push(T item){
	queue<T> *targetQ = NULL;

	if ( !queue1->empty() ) targetQ = queue1;
	else if ( !queue2->empty() ) targetQ = queue2;
	else targetQ = queue1;

	targetQ->push(item);

	return 1;
}

int main(){
	int item;
	QueueWithTwoStacks<int> aQueue;
	StackWithTwoQueues<int> aStack;

	try {
		aQueue.enqueue(1);
		aQueue.enqueue(2);
		aQueue.enqueue(3);
		aQueue.enqueue(4);
		cout << aQueue.dequeue();
		cout << aQueue.dequeue(); 
		cout << aQueue.dequeue();
		cout << aQueue.dequeue();
//		cout << aQueue.dequeue();
		aQueue.enqueue(5);
		cout << endl;

		aStack.push(1);
		aStack.push(2);
		aStack.push(3);
		aStack.push(4);
		aStack.push(5);
		cout << aStack.pop();
		cout << aStack.pop();
		cout << aStack.pop();
		cout << aStack.pop();
		cout << aStack.pop();
		cout << aStack.pop();
		cout << aStack.pop();


	} catch ( const underflow_error e) {
		cout << e.what() << endl;
	}

	return 0;
}
