#include <iostream>

using namespace std;

int reverse(char* pStr, int start, int end){
	char temp;

	if ( pStr==NULL ) return 0;
	if ( start > end ) return 0;

	while (start<end){
		temp = *(pStr+start);
		*(pStr+start) = *(pStr+end);
		*(pStr+end) = temp;
		start++;
		end--;
	}

	return 1;
}

int getLen(char* pStr){

	int i=0;
	if ( pStr==NULL ) return 0;

	while ( *pStr!='\0' ){
		pStr = pStr+1;
		i++;
	}

	return i;
}

int rotateSentence(char* pStr,int cnt){

	int start=0, end=0, i=0;
	int len=0;

	if ( pStr==NULL || cnt <0 ) return 0;

	len = getLen(pStr);
	cout << "len: " << len << endl;
	if (len<=0) return 0;

	reverse(pStr, 0, len-1);

	reverse(pStr, start, len-cnt-1);
	reverse(pStr, len-cnt, len-1);

	return 1;
}

int main(){
	char str[] = "abcdefghijk";
	
	cout << str << endl;

	rotateSentence(str,3);

	cout << str << endl;

	return 0;
}
