#include <iostream>
//#include <cstring>

using namespace std;

//c++ style struct
struct ListNode {
	ListNode* m_nNext;
	int m_nValue;
};

ListNode* checkLoop(ListNode *pHead);

int insertListNode(ListNode** pHead, int value) {
	ListNode *tempNode=NULL;

	//tempNode = (ListNode*)malloc(sizeof(ListNode));
	tempNode = new ListNode;

	if (tempNode==NULL) return 0;

	tempNode->m_nValue = value;
	tempNode->m_nNext = NULL;

	if ( *pHead == NULL ) {
		*pHead = tempNode;
	} else {
		tempNode->m_nNext = *pHead;	
		*pHead = tempNode;
	}

	return 1;
}

int freeListNodeAll(ListNode** pHead){
	ListNode *tempNode=NULL;
	ListNode *pLoopEntry = NULL;

	//check loop
	pLoopEntry = checkLoop( (*pHead) );

	while (*pHead!=NULL){
		tempNode = *pHead;
		*pHead = (*pHead)->m_nNext;

		// if tempNode is same for entry loop node
		// skip after change m_nNext to NULL
		if ( pLoopEntry!=NULL && tempNode == pLoopEntry ) tempNode->m_nNext = NULL;
		else delete tempNode;
	}
	return 0;
}

int PrintList(ListNode* pHead){
	int cnt=0;
	while ( pHead!=NULL ){
		cnt++;
		if (cnt>20) break;
		cout << pHead->m_nValue << "\t" ;
		pHead = pHead->m_nNext;
	}
	cout << endl;

	return 1;
}


int MakeALoop(ListNode **pHead, int value){

	ListNode *tempNode = *pHead;
	ListNode *targetNode = NULL;

	if ( pHead==NULL || *pHead==NULL ) return 0;

	while ( tempNode!=NULL ) {
		if ( tempNode->m_nValue == value ) targetNode = tempNode;

		if ( tempNode->m_nNext == NULL ) break;
		else tempNode = tempNode->m_nNext;
	}

	if ( targetNode == NULL ) return 0;

	tempNode->m_nNext = targetNode;

	return 1;
}

ListNode* checkLoop(ListNode *pHead){
	ListNode *pTortoise = NULL;
	ListNode *pTempNode = NULL;
	ListNode *pEntryNode = NULL;
	ListNode *pHare = NULL;

	if ( pHead == NULL ) return 0;

	pTortoise = pHead;
	pHare = pTortoise->m_nNext;

	while ( pTortoise != pHare ) {

		if ( pTortoise->m_nNext == NULL ) return 0;
		pTortoise = pTortoise->m_nNext;	

		if ( pHare->m_nNext == NULL ) return 0;
		pHare = pHare->m_nNext;

		if ( pHare->m_nNext == NULL ) return 0;
		pHare = pHare->m_nNext;

	}

	// count loop
	int loopCnt=1;
	pTempNode = pTortoise->m_nNext;
	while ( pTortoise != pTempNode ){
		loopCnt++;
		pTempNode = pTempNode->m_nNext;	
	} 
	cout << "Loop cnt " << loopCnt << endl;

	// get Entry Node
	pTortoise = pEntryNode = pHead;
	while ( loopCnt>0 ) {
		pEntryNode = pEntryNode->m_nNext;	
		loopCnt--;
	}
	cout << "Entry Node " << pEntryNode->m_nValue << endl;

	while ( pTortoise != pEntryNode ) {
		pTortoise = pTortoise->m_nNext;
		pEntryNode = pEntryNode->m_nNext;
	}

	cout << "Entry Node " << pEntryNode->m_nValue << endl;
	if ( pEntryNode != NULL ) return pEntryNode;
	else return 0;

}

int main(){

	ListNode* pHead=NULL;

	insertListNode(&pHead, 4);
	insertListNode(&pHead, 8);
	insertListNode(&pHead, 6);
	insertListNode(&pHead, 1);
	insertListNode(&pHead, 2);
	insertListNode(&pHead, 0);
	insertListNode(&pHead, 9);

	PrintList(pHead);
	cout << "Loop " << checkLoop(pHead) << endl;

	MakeALoop(&pHead,2);
	cout << "Loop " << checkLoop(pHead) << endl;
		
	freeListNodeAll(&pHead);

	return 0;
}
