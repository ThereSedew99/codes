#include <iostream>

using namespace std;

struct BinaryListNode {
	BinaryListNode *right;
	BinaryListNode *left;
	int value;
};

bool getBSTMaxSize(BinaryListNode *pNode, int *size, int *min, int *max){
	int ret = 0;

	// what should do from leaft node
	if ( pNode == NULL ) {
		*min = 0x7fffffff;
		*max = 0x80000000;
		*size = 0;
		return true;
	} else if ( pNode->left == NULL && pNode->right == NULL ) {
		*min = *max = pNode->value;
		*size = 1;
		return true;
	}

	int leftSize, leftMin, leftMax;
	int isLeftBST = getBSTMaxSize(pNode->left, &leftSize, &leftMin, &leftMax);

	int rightSize, rightMin, rightMax;
	int isRightBST = getBSTMaxSize(pNode->right, &rightSize, &rightMin, &rightMax);

	if ( isLeftBST && isRightBST && leftMax <= pNode->value && pNode->value <= rightMin ){
		*size = leftSize+rightSize+1;
		*min = ( pNode->value < leftMin ) ? pNode->value : leftMin;
		*max = ( pNode->value > rightMax ) ? pNode->value : rightMin;
		ret = 1;
	} else {
		*size = ( leftSize > rightSize ) ? leftSize : rightSize;
		ret = 0;
	}

	return ret;
}

int main(){
	int size=0, min=0, max=0;

	cout << "BST " << getBSTMaxSize(NULL, &size, &min, &max);	
	cout << " size " << size << " min " << min << " max " << max << endl;
	return 0;

}
