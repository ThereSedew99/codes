#include <iostream>

using namespace std;

int seqSum(int value){

	unsigned int start = 0, end = 1, sub=0;
	unsigned int cnt=1;
	int check = 1;
	
	if ( value <= 0 ) return 0;

	while( (end<value) && (start<end) ){

		sub = sub+check;
		//cout << sub << "\t";	
		if ( sub == value ) {
			cout << start+1 << "~" << end << endl;
			if ( cnt==2 ) return 1;
		} 

		if ( sub > value ) {
			start = start+1;
			check = start*(-1);
			cnt--;
		} else {
			end = end+1;
			check = end;
			cnt++;
		}
	
	}
	return 0;
}

int main(){

	int value=0;
	int ret=0;

	//value = -1;
	//value = 0;
	value = 15;

	ret = seqSum(value);
	cout << "ret:" << ret << endl;;

	return 0;
}
