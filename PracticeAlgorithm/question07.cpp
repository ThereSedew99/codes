#include <iostream>

#define rowNum 3
#define colNum 4

using namespace std;

int searchMatrix(int *pMatrix,int rowCnt, int colCnt, int value){
	int row=0, col=0, v=0;
	int start=0, end=rowCnt*colCnt-1, mid=0;

	if ( pMatrix == NULL ) return -1;

	// matrix can be manupulated as like and array
	while ( start<end ){
		mid = (start+end)/2;
		row = mid/colCnt;
		col = mid%colCnt;
		v = *(pMatrix+(row)*colCnt+col);
		if ( v < value ) start = mid+1;
		if ( v > value ) end = mid-1;
		else return 1;

	} 

	return 0;
}


int main(){
	int aMatrix[rowNum][colNum] = { {1,3,4,5}, {7,9,10,11}, {13,14,15,17} };

	cout << "return :" << searchMatrix((int*)aMatrix, rowNum, colNum, -1) << endl;
	cout << "return :" << searchMatrix((int*)aMatrix, rowNum, colNum, 17) << endl;

	return 0;
}
