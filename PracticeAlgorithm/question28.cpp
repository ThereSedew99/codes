#include <iostream>

using namespace std;

int turningIndex(int *array, int cnt){
	int midIndex=0;
	int startIndex=0;
	int endIndex = cnt-1;
	int prevValue = 0;
	int nextValue = 0;
	int midValue = 0;

	if ( array == NULL ) return -1;

	while ( startIndex < endIndex-1) {
		midIndex = (startIndex+endIndex)/2;
		if ( midIndex <=0 || midIndex >= cnt-1 ) return -1;

		midValue = *(array+midIndex);
		prevValue = *(array+midIndex-1);
		nextValue = *(array+midIndex+1);

		if ( midValue > prevValue && midValue > nextValue ) return midIndex;
		if ( midValue > prevValue && midValue < nextValue ) startIndex = midIndex;
		if ( midValue < prevValue && midValue > nextValue ) endIndex = midIndex;
	}	

	return -1;
}

int main(){
	//int array[] = { 10,9,8,7,6,5,4 };
	//int array[] = { 1,2,3,4,5,10 };
	int array[] = { 1,2,3,4,5,10,9,8,7,6 };

	int size = sizeof(array) / sizeof(int);

	cout << turningIndex(array, size);

	return 0;
}
