#include <iostream>

using namespace std;


// sort coins

int minNumCoins(int target, int *coins, int cnt){

	unsigned int minCnt = 0xFFFF;
	unsigned int maxCoinCnt = 0;
	int remainTarget = 0;

	if ( cnt <=0 ) return 0;

	maxCoinCnt = target / *(coins+(cnt-1));

	cout << "- target:" << target << " coin:" << *(coins+(cnt-1)) << " CoinsCnt:" << cnt << " maxCoinCnt:" << maxCoinCnt<< endl;

	if ( cnt==1 ) {

		return maxCoinCnt;
	}

	int sumCoinCnt = 0;


	for (int i=0; i<=maxCoinCnt; i++){
		remainTarget = target - i*(*(coins+(cnt-1)));

		sumCoinCnt = i + minNumCoins( remainTarget, coins, cnt-1);
		if ( sumCoinCnt!=0 && sumCoinCnt < minCnt ) minCnt = sumCoinCnt;
		cout << "* maxTargetCoinCnt:" << i << " coin:"<< *(coins+(cnt-1)) << " remain:" <<remainTarget << " sumCoinCnt:" << sumCoinCnt << " minCnt:" << minCnt <<endl;
	}

	return minCnt;
}

int main(){

	int coins[] = {1,3,9,10};
	int cnt = sizeof(coins) / sizeof(int);

	cout << minNumCoins(15, coins, cnt);	

	return 0;
}
