#include <iostream>
#include <cstring>

using namespace std;

bool strPathCore(char* array, int row, int col, int rowIndex, int colIndex, char *str, int &strIndex, bool *visited){
	bool isVisited = false;
	char target = 0;
	bool goTarget = false;

	if ( array == NULL || str == NULL ) return false;

	if ( *(str+strIndex) == '\0' ) return true;
	if ( rowIndex<0 || colIndex<0 || rowIndex>=row || colIndex>=col ) return false;

	target   = *(array+rowIndex*col+colIndex);
	isVisited = *(visited+rowIndex*col+colIndex);

	//cout << "? " << target << "/" << *(str+strIndex) << "=" << isVisited << endl;
	if ( (isVisited==false) && (target==*(str+strIndex)) ) {
		*(visited+rowIndex*col+colIndex) = true;
		strIndex=strIndex+1;
		goTarget = strPathCore(array, row, col, rowIndex, colIndex+1, str, strIndex, visited)  // goRight
		        || strPathCore(array, row, col, rowIndex, colIndex-1, str, strIndex, visited)  // goLeft
		        || strPathCore(array, row, col, rowIndex-1, colIndex, str, strIndex, visited)  // goUp
		        || strPathCore(array, row, col, rowIndex+1, colIndex, str, strIndex, visited);  // goDown

		if (goTarget==true){
			//cout << "* " << target << "/" << *(str+strIndex) << endl;
			return true;
		}
		else {
			//cout << "-" << endl;
			strIndex=strIndex-1;	
			*(visited+rowIndex*col+colIndex) = false;
		}
	}

	return false;
}

bool stringPath(char *array, int row, int col, char *str){
	bool *visited = new bool[row*col]();
	int strIndex = 0;
	bool ret=false;

	if ( array==NULL || str==NULL ) return false;

	for (int rowIndex=0; rowIndex<row; rowIndex++){
		for (int colIndex=0; colIndex<col; colIndex++){
			if ( *(array+rowIndex*col+colIndex) == *(str+0) ){
				ret = strPathCore(array, row, col, rowIndex, colIndex, str, strIndex, visited);
				if ( ret == true ) return true;
			}
		}
	}

	delete[] visited;
	return false;
}

int main(){
	int rows=3, cols=4;

	char *matrix = new char[rows*cols+1]{ 'a','b','c','e', 's','f','c','s', 'a','d','e','e','\0' };
	char *str = new char[6]();

	strcpy(str,"abcb");
	*(str+4) = '\0';

	cout << stringPath(matrix, rows, cols, str) << endl;

	delete[] matrix;
	delete[] str;

	return 0;
}

