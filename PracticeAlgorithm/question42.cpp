#include <iostream>

bool isAllDigits(char* num){
	char* temp = num;
	if (num==NULL) return false;
	if ( *temp == '\0' ) return false;

	while ( *temp != '\0' ) {
		if ( *temp<'0' && *temp>'9') return false;
		temep++;
	}
	return true;
}


int reverse(char *num){
	char* temp = NULL;
	int idxStart=0; idxEnd=0;

	if (num==NULL) return -1;

	while (num[idxEnd]!='\0') idxEnd++;

	while ( idxStart < idxEnd ){
		temp= num[idxEnd];
		num[idxEnd] = num[idxStart];
		num[idxStart] = temp;

		idxStart++;
		idxEnd--;
	}

	return 0;
}

int add(char *num1, char *num2, char *sum){
	int index1=0, index2=0, idexSum=0;

	if ( num1==NULL || num2==NULL || sum==NULL ) return -1;
	
	if ( !isAllDigits(num1) ) return -1;
	if ( !isAllDigits(num2) ) return -1;
	
	while (num1[index1]!='\0') index1++;
	index1--;
	while (num1[index2]!='\0') index2++;
	index2--;

	for ( idxSum=0; index1>=0||index2>=0 ; idxSum++){
		int digit1=0; digit2=0;

		digit1 = ( index1<0 ) ? 0 : num1[index1]; 
		digit2 = ( index2<0 ) ? 0 : num2[index2]; 

		sum[idxSum] += digit1-'0' + digit2-'0' ;
		if ( sum[idxSum] > 10 ) sum[idxSum+1]++;
		sum[idxSum] += '0';

		if ( index1 >=0 ) index1--;
		if ( index2 >=0 ) index2--;
	}
	sum[idxSum] = '\0';

	return 0;
}
