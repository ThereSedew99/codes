#include <iostream>

BinaryTreeNode* findKth(BinaryTreeNode *pNode, int k, int *pCnt){

	BinaryTreeNode* target = NULL;

	if ( pNode == NULL || k<=0) return NULL;

	if ( pNode->left ) {
		target = findKth(pNode->left, k, pCnt);
	}

	if ( target==NULL ){
		*pCnt = *pCnt+1;
		if ( *pCnt == k ) target = pNode;
	}

	if ( target==NULL && pNode->right ){
		target = findKth(pNode->right, k, pCnt);
	}
	
	return target;
}
