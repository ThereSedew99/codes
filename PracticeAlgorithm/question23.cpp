#include <iostream>

using namespace std;

long long fibonacciR(unsigned int n){

	if ( n<=0 ) return 0;
	if ( n==1 ) return 1;

	return fibonacciR(n-1) + fibonacciR(n-2);
}

long long fibonacciI(unsigned int n){

	long long fibMinusOne;
	long long fibMinusTwo;
	long long fibN;

	if (n<=0) return 0;
	if (n==1) return 1;

	fibMinusOne = 1;
	fibMinusTwo = 0;

	for (unsigned int i=2;i<=n;i++){
		fibN = fibMinusOne + fibMinusTwo;

		fibMinusTwo = fibMinusOne;
		fibMinusOne = fibN;
	}

	return fibN;
}

int main() {

	cout << "Fib " << fibonacciR(8) << endl;
	cout << "Fib " << fibonacciI(8) << endl;

}
