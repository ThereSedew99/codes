#include <iostream>

using namespace std;

int leftTarget(int *array, int arraySize, int target, int start, int end);
int rightTarget(int *array, int arraySize, int target, int start, int end);

int getOccurrences(int* array, int arraySize, int target){
	int leftIndex=0;
	int rightIndex=0;

	if ( (array==NULL) || (arraySize<=0) ) return 0;

	leftIndex = leftTarget(array, arraySize, target, 0, arraySize-1);
	if ( leftIndex == -1 ) return 0;

	rightIndex = rightTarget(array, arraySize, target, leftIndex+1, arraySize-1);

	if ( rightIndex == -1 ) return 1;
	else return (rightIndex-leftIndex+1);
	
}

int leftTarget(int *array, int arraySize, int target, int start, int end){
	if ( (start>end) || (end<start) ) return -1;

	int mid=(start+end)/2;
	int midValue = *(array+mid);

	cout << "l :" << mid << "-" << midValue << endl;
	if ( midValue == target){
		if ( (mid==0) || *(array+mid-1) < target ) return mid;
		end = mid-1;
	}
	else if ( midValue < target ) {
		start = mid+1;
	} else {
			end = mid-1;
	}

	return leftTarget(array, arraySize, target, start, end); 
}

int rightTarget(int *array, int arraySize, int target, int start, int end){
        if ( (start>end) || (end<start) ) return -1;

        int mid=(start+end)/2;
        int midValue = *(array+mid);

        cout << "r :" << mid << "-" << midValue << endl;
	if ( midValue == target ) {
                if ( (mid==(arraySize-1)) || *(array+mid+1) > target ) return mid;
		start = mid+1;

	}
        else if( midValue < target ) {
		start = mid+1;
        } else { 
		end = mid-1;
        }

	return rightTarget(array, arraySize, target, start, end);
}
nt leftTarget(int *array, int arraySize, int target, int start, int end){

        if ( (start>end) || (end<start) ) return -1;

        int mid=(start+end/2);
        int midValue = *(array+mid);

        cout << mid << "-" << midValue << endl;
        if ( midValue < target ) {
                return findTarget(array, arraySize, target, mid+1, end);
        } else if ( midValue > target ){
                return findTarget(array, arraySize, target, start, mid-1);
        } else {
                if ( *(array+mid-1) < target ) return mid;
                else {
                        return findTarget(array, arraySize, target, start, mid-1);
                }
        }

}

int main(){
	//int array[] = { 1,2,2,3,3,3,3,3,3 };
	//int array[] = { 1,2,3,3,3,3,4,5 };
	//int array[] = { 3,3,3,3,4,5,6,7 };
	//int array[] = { 4,5,6,7 };
	int array[] = { };
	int size = sizeof(array)/sizeof(int);

	cout << getOccurrences(array, size, 3);

	return 0;
}
