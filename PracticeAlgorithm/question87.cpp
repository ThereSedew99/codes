#include <iostream>

using namespace std;


bool findPairedSum(int* pArray, int sizeArray, int s){
	int start=0;
	int end=sizeArray-1;
	int sum=0;

	if ( pArray == NULL ) return false;

	while ( start < end ) {
		sum = *(pArray+start) + *(pArray+end);

		if ( sum == s ) {
			cout << *(pArray+start) << "+" << *(pArray+end) << "=" << s <<endl;
			return true;
		}
		else if ( sum < s ) start++;
		else end--;
	}

	return false;
}


int main(){
	//int array[] = {15};
	//int array[] = {1,2,7,11,15};
	int array[] = {1,2,4,7,11,15};
	//int array[] = {};
	int size = sizeof(array)/sizeof(int);

	cout << findPairedSum(array, size, 15);

	return 0;
}
