#include <stdio.h>
#include <stdlib.h> // malloc
#include <string.h> // strcpy

#define MAXLEN 30

// const pointer can change the address but cannot change data
int replaceBlank(char* str,const char * target){
	int blankCnt = 0;
	int originalLen = 0;
	int newLen=0;
	int targetLen=0;

	// inital error handle
	if ( str == NULL || target == NULL ) return -1;
	originalLen = strlen(str);
	targetLen = strlen(target);

	//find blank str count
	for (int i=0 ; i<originalLen ; i++){
		if (*(str+i) == ' ')  blankCnt++;
	}

	//calculate available space
	newLen = originalLen + blankCnt*(strlen(target)-1);

	if (newLen > MAXLEN) return -1;
	if (blankCnt <=0) return -1;

	printf ("[%s] << [%s/%d] %d > %d(%d) \n", str, target,targetLen, originalLen, newLen, blankCnt);

	//perform the manipulation
	char *originalPtr = NULL;
	char *newPtr = NULL;
	originalPtr = str+(originalLen-1);
	newPtr = str+(newLen-1);

	for (int i=0 ; i<originalLen ; i++){

		if ( *originalPtr==' ' ) {
			// copy target
			for (int j=0;j<targetLen;j++){
				*newPtr = *(target+(targetLen-1)-j);
				newPtr = newPtr-1;
			}
		} else {
			*newPtr = *originalPtr;
			newPtr=newPtr-1;
		}
		originalPtr=originalPtr-1;

		printf("* [%s] %d \n",str,i);
	}
	//return error or success
	return 1;
}


int main(){
	char* string = NULL;
	string = (char*) malloc(sizeof(char)* MAXLEN);

	if ( string != NULL ) {
		memset(string, '\0', MAXLEN);
		strcpy ( string, " We are happy." );
		printf ("%s\n", string);

		//case1
		replaceBlank(string, "%20");
		//case2
		replaceBlank(string,NULL);
		//case3
		replaceBlank(NULL, "%20");

		//case3
		memset(string, '\0', MAXLEN);
		//strcpy ( string, "          " );
		strcpy ( string, " " );
		replaceBlank(string, "%20");

		//case4
		memset(string, '\0', MAXLEN);
		strcpy ( string, "1234567890" );
		replaceBlank(string, "%20");

		printf("[%s]\n",string);
	}

	if (string != NULL) free(string);
	return 0;
}
