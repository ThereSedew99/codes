#include <iostream>
#include <cstring>

using namespace std;

bool increment(char* number, int digit){
	bool isOverflow = false;

	number[digit-1] = number[digit-1] + 1;
	for (int i=digit-1; i>=0 ;i--){
		if ( (number[i] - '0') >= 10 ) {
			if ( i==0 ) {
				isOverflow = true;
				break;
			}
			number[i-1] = number[i-1]+1;	
			number[i] = '0';
		}
	}

	return isOverflow;
}

void printDigits(char * number){
	char* pChar = number;

	if ( number == NULL ) return;

	while ( *pChar == '0' ){
		pChar = pChar+1;
	}

	cout << pChar << endl;
}


int printAllDigits(int digit){
	if ( digit <=0 ) return -1;

	char* number = new char[digit+1];
	memset(number,'0', digit);
	number[digit] = '\0';

	while (!increment(number, digit)){
		printDigits(number);
	}

	delete number;

	return 0;
}

int main(){

	printAllDigits(100);

	return 0;
}
