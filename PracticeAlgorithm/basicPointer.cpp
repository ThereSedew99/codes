#include <iostream>

using namespace std;

int aFunc(int a){
	cout << "pFunc "<< a <<endl;
	cout << &a <<endl;
//	cout << *a <<endl;
	a = 4;
	return 0;
}

int paFunc(int *a){
	cout << "paFunc "<< a <<endl;
	cout << "paFunc& "<<&a <<endl;
	cout << "paFunc* "<<*a <<endl;
	*a = 5;
	return 0;
}

int ppaFunc(int **a){
	cout << "ppaFunc " << a << endl;
	cout << "ppaFunc& "<<&a <<endl;
	cout << "ppaFunc* "<<*a <<endl;
	*(*a) = 10;
	return 0;
}

int main(){

	int a = 3;
	cout << "a "<< a <<endl;
	cout << "&a "<<&a <<endl<<endl;
//	cout << *a <<endl;

	int b[1] = { 1 };
	int *c = b;

	cout << "b "<<b <<endl;
	cout << "&b "<<&b <<endl<<endl;
	//cout << *b <<endl;
	cout << "c "<<c <<endl;
	cout << "&c "<<&c <<endl;
	cout << "*c "<<*c <<endl<<endl;

	cout << "Main1 a" << endl;
	aFunc(a);
	cout << "Main1 "<< a <<endl<<endl;

	cout << "Main2 &a" << endl;
	paFunc(&a);
	cout << "Main2 "<< a <<endl<<endl;

	cout << "Main3 c" << endl;
	paFunc(c);
	cout << "Main3 "<< c <<endl;
	cout << "Main3& "<< &c <<endl;
	cout << "Main3* "<< *c <<endl<<endl;


	cout << "Main4 &c" << endl;
	ppaFunc(&c);
	cout << "Main4 "<< c <<endl;
	cout << "Main4& "<< &c <<endl;
	cout << "Main4* "<< *c <<endl;
}
